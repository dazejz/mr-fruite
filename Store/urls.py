from django.urls import path,re_path
from Store.views import *

urlpatterns = [
    #登录逻辑
    path('index/', index),
    path('login/', login),
    path('register/', register),
    path('logout/', logout),
    #商品类型逻辑
    path('goods_types/', goods_types),
    path('add_type/', add_type),
    re_path('del_type/(?P<id>\d+)/', del_type),
    re_path('edit_type/(?P<id>\d+)/', edit_type),
    #店铺逻辑
    path('store/', store),
    path('add_store/', add_store),
    path('add_store_ajax/', add_store_ajax)
]