from django.db import models

class User(models.Model):
    username = models.CharField(max_length = 32,unique = True)
    password = models.CharField(max_length = 32)
    user_type = models.CharField(max_length = 8,default = "a") #a admin v vip u simple user

class Store(models.Model):
    store_name = models.CharField(max_length = 32)
    store_address = models.TextField(default="")
    store_description = models.TextField(default="")
    store_picture = models.ImageField(upload_to = "image")

    store_user = models.OneToOneField(to=User, on_delete=models.CASCADE)

class GoodsType(models.Model):
    type_name = models.CharField(max_length = 32)
    type_picture = models.ImageField(upload_to = "image") #下载pillow  配置upload

    def __str__(self):
        return self.type_name

# Create your models here.
