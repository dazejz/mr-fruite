# Generated by Django 2.2.1 on 2021-05-24 02:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Store', '0002_auto_20210521_1531'),
    ]

    operations = [
        migrations.AddField(
            model_name='store',
            name='store_picture',
            field=models.ImageField(default='image/1.jpg', upload_to='image'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='user',
            name='user_type',
            field=models.CharField(default='a', max_length=8),
        ),
    ]
