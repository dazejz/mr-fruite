import hashlib

from django.shortcuts import render,redirect
from django.http import JsonResponse

from Store.models import *

def setPassword(password):
    md5 = hashlib.md5()
    md5.update(password.encode())
    result = md5.hexdigest()
    return result

def loginValid(fun):
    def inner(request,*args,**kwargs):
        username = request.COOKIES.get("username")
        id = request.COOKIES.get("id")
        user = User.objects.filter(username = username,id = id).first()
        if user:
            return fun(request,*args,**kwargs)
        else:
            return redirect("/Store/login/")
    return inner

@loginValid
def index(request):
    return render(request,"store/index.html")

def login(request):
    if request.method == "POST":
        username = request.POST.get("username")
        password = request.POST.get("password")
        not_empty = username and password  # 不为空
        if not_empty:
            user = User.objects.filter(username = username).first()
            if user:
                if user.password == setPassword(password):
                    response = redirect("/Store/index/")
                    response.set_cookie('username',username)
                    response.set_cookie('id',user.id)
                    return response
    return render(request,"store/login.html")

def register(request):
    if request.method == "POST":
        username = request.POST.get("username")
        password = request.POST.get("password")
        password_again = request.POST.get("password_again")

        not_empty = username and password and password_again #不为空
        same_password = password == password_again #两次密码一致

        if not_empty and same_password:
            user = User()
            user.username = username
            user.password = setPassword(password)
            user.save()
            return redirect("/Store/login")
    return render(request,"store/register.html")

def logout(request):
    response = redirect("/Store/login/")
    response.delete_cookie("username")
    response.delete_cookie("id")
    return response

#商品的类型
#商品类型列表
def goods_types(request):
    t_list = GoodsType.objects.all()
    return render(request,"store/goods_types.html",locals())

#商品类型添加
def add_type(request):
    if request.method == "POST":
        t_name = request.POST.get("t_name")
        t_picture = request.FILES.get("t_picture")

        g = GoodsType()
        g.type_name = t_name
        g.type_picture = t_picture
        g.save()

    return redirect("/Store/goods_types/")

#商品类型删除
def del_type(request,id):
    t = GoodsType.objects.filter(id = id).first()
    if t:
        t.delete()
    return redirect("/Store/goods_types/")

#商品类型修改
def edit_type(request,id):
    t = GoodsType.objects.filter(id=id).first()
    if request.method == "POST":
        t_name = request.POST.get("t_name")
        # t_picture = request.FILES.get("t_picture")
        t.type_name = t_name
        # t.type_picture = t_picture
        t.save()
        return redirect("/Store/goods_types/")
    return render(request,"store/edit_goods_types.html",locals())

#店铺管理部分
#店铺展示逻辑
@loginValid
def store(request):
    user = User.objects.filter(id = request.COOKIES.get("id")).first()
    try:
        s = user.store
    except:
        s = {}
    return render(request,'store/store.html',locals())
@loginValid
def add_store(request):
    return render(request,'store/add_store.html',locals())

@loginValid
def add_store_ajax(request):
    result = {"code": 200,"data": ""}
    if request.method == "POST":
        store_name = request.POST.get("store_name")
        store_address = request.POST.get("store_address")
        store_description = request.POST.get("store_description")
        store_picture = request.FILES.get("store_picture")

        store = Store()
        store.store_name = store_name
        store.store_address = store_address
        store.store_description = store_description
        store.store_picture = store_picture

        store.store_user = User.objects.get(id = request.COOKIES.get("id"))
        store.save()

        result["data"] = {
            "id": store.id,
            "store_name": store.store_name,
            "store_address": store.store_address,
            "store_description": store.store_description,
            "store_picture": store.store_picture.url
        }
    else:
        result["code"] = 500
        result["data"] = "请求类型必须是POST请求"
    return JsonResponse(result)
















# Create your views here.
